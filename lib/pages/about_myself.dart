
import 'package:flutter/material.dart';

class AboutMyself extends StatefulWidget {
  const AboutMyself({Key? key}) : super(key: key);

  @override
  State<AboutMyself> createState() => _AboutMyselfState();
}

class _AboutMyselfState extends State<AboutMyself> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Text(
              'นายนราวิชญ์ กิจวงศ์วัฒนา',
              style: TextStyle(height: 2, fontSize: 30),
            ),
            Text(
              '6350110010',
              style: TextStyle(height: 1, fontSize: 20),
            ),
            Image.asset('asset/1.jpg'),

            Text(
              'นายณัฐชนน มณียม',
              style: TextStyle(height: 2, fontSize: 30),
            ),
            Text(
              '6350110004',
              style: TextStyle(height: 1, fontSize: 20),
            ),
            Image.asset('asset/2.jpg'),
          ],
        ),
      ),
    );
  }
}
