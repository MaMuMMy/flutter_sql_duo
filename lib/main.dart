import 'dart:io';
import 'package:flutter/material.dart';
import 'package:sqlite_basic/pages/list_profile.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SQLite Demo',
      theme: ThemeData(
        primarySwatch: Colors.brown,
      ),
      home: const ListProfile(title: 'Car deposit list'),
      debugShowCheckedModeBanner: false,
    );
  }
}




