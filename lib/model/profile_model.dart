
class ProfileModel {
  int? id;
  String firstname_lastname;
  String brand;
  String license_plate;
  String phone;
  String image;

  ProfileModel({
    this.id,
    required this.firstname_lastname,
    required this.brand,
    required this.license_plate,
    required this.phone,
    required this.image,

  });

  factory ProfileModel.fromMap(Map<String, dynamic> json) =>
      new ProfileModel(
        id: json['id'],
        firstname_lastname: json['firstname'],
        brand: json['lastname'],
        license_plate: json['email'],
        phone: json['phone'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'firstname': firstname_lastname,
      'lastname': brand,
      'email': license_plate,
      'phone': phone,
      'image': image,
    };
  }
}